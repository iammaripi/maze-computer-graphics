# **Trapped in a Maze**

Este es el proyecto para la asignatura Gráficos por Computador de la Escuela Superior de Informática de Ciudad Real [ESI](http://webpub.esi.uclm.es/).

### **¿Qué es Trapped in a Maze?** ###

Es un juego de laberintos en 3D. Está desarrollado en Blender.

### **Documentación** ###

Puede encontrar toda la documentación necesaria en las diferentes secciones de la [wiki del proyecto](https://bitbucket.org/iammaripi/maze-computer-graphics/wiki/Home).